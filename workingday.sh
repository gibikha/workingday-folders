#!/usr/bin/env bash

##
# Simple script to create a directory structure in the form of:
# mm-MM/dd-DD (00-Month/00-Day)
#
# I tried to make the variable names as obvious as possible
#
# For the purpose of _this_ script, it creates working days 
# for a medical practice, Monday to Saturday.
# The months and days are numbered so that the sort order is preserved
# nicely on the M$ Windows machines in which they will be accessed.
##

## start_date is the first paramater passed to the script from the CLI
## must be properly formatted: eg. YYYY-MM-DD

start_date=$1

loop_date=$start_date # just to make while loop a little more readable

## add one day to end date, so that the while loop works as expected
end_date=`date -I -d "$2 + 1 day"`

## use start_month in numeric version, eg 01 for January
start_month=`date -d $start_date "+%m"`

## month_directory template using GNU date syntax 00-Month
month_dir=`date -d $start_date "+%m-%B"`

## create directory for month in current directory
mkdir "$month_dir"

## the while loop is comparing strings, so we can't use a integer operator
while [ "$loop_date" != "$end_date" ]; do 
  
    ## set current month to numeric version of the month currently being looped through
    current_month=`date -d $loop_date "+%m"`

    ## if the current month is different from the starting month, let's make the new directory
    if [ "$current_month" != "$start_month" ]; then
        start_month=$current_month
        month_dir=`date -d $loop_date "+%m-%B"`
        mkdir "$month_dir"
    fi

    ## set current day to full name of day, eg. Sunday
    current_day=`date -d $loop_date "+%A"`
    
    ## if not Sunday, let's make a folder within current month, in the format 00-Day
    if [ "$current_day" != "Sunday" ]; then
        current_day=`date -d $loop_date "+%d-%A"`
        mkdir "$month_dir/$current_day"
    fi

    ## add one day to the date we are working with in the loop
    loop_date=$(date -I -d "$loop_date + 1 day")

done
