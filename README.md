# workingday folders

Simple bash (for now) script that creates nested folders for every working day of the months within the range passed to script.

## Getting Started

Make the script executable by running chmod +x workingday.sh

### Prerequisites

GNU bash

GNU coreutils

## Using the script

You can then run it by passing two dates to it: ./workingday.sh 2020-08-01 2020-12-31

The script will then create folders for the months of August to December, with subfolders for all of the days of the week that are not Sunday.

## Authors

* **Gibran Khan**

## License

This project is licensed under 2-clause BSD License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Friends with requests ;)
